<?php
/**
 * @file
 * Rules integration for Sanitizer.
 */

function db_sanitizer_rules_event_info() {
  $events = array();

  $events['db_sanitizer_generation_failed'] = array(
    'label' => t('Generation of Sanitizer script failed.'),
    'group' => t('Sanitizer'),
    'access callback' => array('administer site configuration'),
    'variables' => array(
      'message' => array(
        'label' => t('Message'),
        'type' => 'text',
      ),
    ),
  );

  return $events;
}
