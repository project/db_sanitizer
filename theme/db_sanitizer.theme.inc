<?php
/**
 * @file
 * Theme functions.
 */


/**
 * Administration form theme function
 */
function theme_db_sanitizer_table_config_edit_form_commands($variables) {
  $form = $variables['form'];

  $header = array(t('Remove'), t('Type'), t('Set'), t('Condition'), t('Condition 2'));

  $rows = array();
  foreach (element_children($form) as $key) {
    $row = array();
    $row[] = drupal_render($form[$key]['remove']);
    $row[] = drupal_render($form[$key]['type_label']);
    $row[] = drupal_render($form[$key]['set']);
    $row[] = drupal_render($form[$key]['condition']);
    $row[] = drupal_render($form[$key]['condition2']);
    $rows[] = array('data' => $row);
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);
  return $output;
}
