<?php
/* 
 * @file
 * Drush integration for Sanitizer.
 */

/**
 * Implements hook_drush_command().
 */
function db_sanitizer_drush_command() {
  $items['db-sanitizer-create-sql'] = array(
    'description' => 'Create sql file.',
    'arguments' => array(
      'name' => array('description' => 'Name of configuration'),
      'filename' => array('description' => 'Filename where sql should be stored'),
    ),
    'examples' => array(
      'drush db-sanitizer-create-sql default /tmp/db_sanitizer.sql' => 'Creates db_sanitizer.sql file.',
    ),
  );

  $items['db-sanitizer-sanitize'] = array(
    'description' => "Run sanitization operations on the current database.",
    'arguments' => array(
      'name' => array('description' => 'Name of configuration'),
    ),
    'callback' => '',
    'examples' => array(
      'drush db-sanitize default' => 'Sanitize current database using default configuration.',
    ),
    'aliases' => array('db-sanitize', 'dbsqlsan'),
  );

  return $items;
}

/**
 * Command db_sanitizer-create-sql.
 * @param <string> $filename
 *   Path to file.
 */
function drush_db_sanitizer_create_sql($name, $filename) {
  $output = db_sanitizer_generate_sql($name);
  if (is_array($output)) {
    // Errors.
    foreach ($output as $error) {      
      drush_set_error(DB_SANITIZER_WATCHDOG, dt($error['message'], $error['variables']));
    }    
  }
  else {
    if (!file_put_contents($filename, $output)) {
      drush_set_error(DB_SANITIZER_WATCHDOG, dt('Can not write to file !filename', array('!filename' => $filename)));
    }
    else {
      drush_log(dt('File !filename has been created successfully', array('!filename' => $filename)), 'success');
    }
  }
}

/**
 * Sanitize database using configuration name.
 *
 * @param $name
 */
function drush_db_sanitizer_sanitize($name) {
  if (empty($name)) {
    return drush_set_error(dt('Please provide valid configuration name.'));
  }

  if (!drush_confirm(dt('Do you really want to sanitize the current database?'))) {
    return drush_user_abort();
  }

  $sanitize_query = db_sanitizer_generate_sql($name);

  $result = FALSE;
  if (!is_array($sanitize_query)) {
    $result = drush_sql_query($sanitize_query);
  }
  
  if ($result) {
    drush_log(dt('Database was sanitized successfully.'), 'success');
  }
  else {
    drush_log(dt('Database was not sanitized.'), 'cancel');
  }

  return $result;
}
