Module helps to create and manage configuration for cleaning database
from critical data.

Idea of module is that it generates sql code to clean entities paying
attention to entity structure, external fields and revisions of entities.

USE CASE (EXAMPLE)
------------------

For example we would like to keep only 100 users in a system. So let's remove all users
that has uid greater 100. Also need to sanitize emails and first and last names of user entity.

So we can create next 4 commands:

user table:
type   | set                                                   | condition
---------------------------------------------------------------------------
update | mail=CONCAT(FLOOR(rand() * 10000000),'@example.com')  | uid > 1    (leave admin email as is)
delete |                                                       | uid > 100

field_data_field_first_name table:
type   | set                                                           | condition
----------------------------------------------------------------------------------
update | field_first_name_value = SUBSTRING(MD5(RAND()) FROM 1 FOR 6)  | uid > 1

field_data_field_last_name table:
type   | set                                                           | condition
-----------------------------------------------------------------------------------
update | field_first_name_value = SUBSTRING(MD5(RAND()) FROM 1 FOR 6)  | uid > 1

DB Sanitizer module generates 10 sql commands based on commands in configuration:

DELETE FROM `users` WHERE uid > 100 AND uid != 0 AND uid != 1;
DELETE FROM `field_data_field_first_name` WHERE entity_type ='user' AND bundle = 'user' AND entity_id NOT IN (SELECT uid FROM `users`);
DELETE FROM `field_data_field_last_name` WHERE entity_type ='user' AND bundle = 'user' AND entity_id NOT IN (SELECT uid FROM `users`);
DELETE FROM `field_revision_field_first_name` WHERE entity_type ='user' AND bundle = 'user' AND entity_id NOT IN (SELECT uid FROM `users`);
DELETE FROM `field_revision_field_last_name` WHERE entity_type ='user' AND bundle = 'user' AND entity_id NOT IN (SELECT uid FROM `users`);
UPDATE `users` SET mail=CONCAT(FLOOR(rand() * 10000000),'@example.com') WHERE uid > 1;
UPDATE `field_data_field_first_name` SET field_first_name_value = SUBSTRING(MD5(RAND()) FROM 1 FOR 6) WHERE entity_id > 1 AND entity_type ='user' AND bundle = 'user';
UPDATE `field_data_field_last_name` SET field_last_name_value = SUBSTRING(MD5(RAND()) FROM 1 FOR 6) WHERE entity_id > 1 AND entity_type ='user' AND bundle = 'user';
UPDATE `field_revision_field_first_name` SET field_first_name_value = SUBSTRING(MD5(RAND()) FROM 1 FOR 6) WHERE entity_id > 1 AND entity_type ='user' AND bundle = 'user';
UPDATE `field_revision_field_last_name` SET field_last_name_value = SUBSTRING(MD5(RAND()) FROM 1 FOR 6) WHERE entity_id > 1 AND entity_type ='user' AND bundle = 'user';


DRUSH INTEGRATION
-----------------

Create file for sanitizing "drush db-sanitizer-create-sql default /tmp/db_db_sanitizer.sql"

Sanitize database "drush db-sanitize default"
