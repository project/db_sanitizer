<?php
/* 
 * @file
 */

/**
 * Configurations form.
 */
function db_sanitizer_configurations_page() {
  $header = array(
    t('Label'),
    array('data' => t('Operations'), 'colspan' => 6),
  );
  $rows = array();

  $db_sanitizer_configurations = db_sanitizer_get_configurations();

  if (!empty($db_sanitizer_configurations)) {
    foreach ($db_sanitizer_configurations as $name => $db_sanitizer_configuration) {
      $row = array(
        $db_sanitizer_configuration['label'],
        l(t('edit'), DB_SANITIZER_PATH . '/manage/' . $name . '/edit'),
        l(t('clone'), DB_SANITIZER_PATH . '/manage/' . $name . '/clone'),
        l(t('delete'), DB_SANITIZER_PATH . '/manage/' . $name . '/delete'),
        l(t('table config'), DB_SANITIZER_PATH . '/manage/' . $name . '/table_config'),
        l(t('entity config'), DB_SANITIZER_PATH . '/manage/' . $name . '/entity_config'),
        l(t('download sql'), DB_SANITIZER_PATH . '/manage/' . $name . '/sql'),
      );
      $rows[] = $row;
    }
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Edit configuration form.
 */
function db_sanitizer_configurations_edit_form($form, &$form_state, $config_name = '', $op = 'add') {
  $db_sanitizer_config = db_sanitizer_get_configurations($config_name);

  if ($op == 'clone') {
    $db_sanitizer_config['label'] .= ' (cloned)';
    $db_sanitizer_config['name'] = '';
  }
  $form['#original_config_name'] = $config_name;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($db_sanitizer_config['label']) ? $db_sanitizer_config['label'] : '',
    '#description' => t('The human-readable name of this configuration.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable name.
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($db_sanitizer_config['name']) ? $db_sanitizer_config['name'] : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'db_sanitizer_configuration_exists',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this configuration. It must only contain lowercase letters, numbers, and underscores.'),
    '#disabled' => !empty($db_sanitizer_config['name']),
  );

  $form['actions'] = array(
    '#type' => 'actions'
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),    
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), DB_SANITIZER_PATH),
  );

  return $form;
}

/**
 * Edit configuration form submit.
 */
function db_sanitizer_configurations_edit_form_submit($form, &$form_state) {
  $name = $form_state['values']['name'];
  $original_name = $form['#original_config_name'];

  $config = array(
    'label' => $form_state['values']['label'],
    'name' => $name,
  );
  if ($original_name != $name) {
    // Clone configuration.
    $table_config = variable_get('db_sanitizer_table_config_' . $original_name);
    $entity_config = variable_get('db_sanitizer_entity_config_' . $original_name);
    variable_set('db_sanitizer_table_config_' . $name, $table_config);
    variable_set('db_sanitizer_entity_config_' . $name, $entity_config);
  }
  db_sanitizer_set_configuration($name, $config);
  drupal_set_message(t('Sanitizer configuration %label has been updated', array('%label' => $form_state['values']['label'])));

  $form_state['redirect'] = DB_SANITIZER_PATH;
}

/**
 * Delete configuration form.
 */
function db_sanitizer_configurations_delete_form($form, &$form_state, $name) {
  $db_sanitizer_configuration = db_sanitizer_get_configurations($name);
  if (empty($db_sanitizer_configuration)) {
    drupal_not_found();
    drupal_exit();
  }
  $form['name'] = array(
    '#type' => 'hidden',
    '#value' => $name,
  );
  return confirm_form($form,
    t('Are you sure you want to delete the configuration %label', array('%label' => $db_sanitizer_configuration['label'])),
    DB_SANITIZER_PATH
  );
}

/**
 * Delete configuration form submit.
 */
function db_sanitizer_configurations_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    db_sanitizer_delete_configuration($form_state['values']['name']);
  }
  $form_state['redirect'] = DB_SANITIZER_PATH;
}

/**
 * Check if name of configuration exists.
 */
function db_sanitizer_configuration_exists($value) {
  $db_sanitizer_configurations = db_sanitizer_get_configurations();
  return isset($db_sanitizer_configurations[$value]);
}

/**
 * Table config page.
 */
function db_sanitizer_table_config_form($form, &$form_state, $config_name) {
  $tables = db_sanitizer_list_tables();
  $entity_tables = db_sanitizer_get_entity_tables();
  $db_sanitizer_conf = db_sanitizer_get_table_config($config_name);
  $statuses = db_sanitizer_get_statuses();

  // Bulk operations.
  $form['operations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Table operations'),
  );
  $form['operations']['status'] = array(
    '#type' => 'select',
    '#title' => t('Set status'),
    '#options' => $statuses,
  );
  $form['operations']['set'] = array(
    '#type' => 'submit',
    '#value' => t('Set'),
    '#submit' => array('db_sanitizer_table_config_form_submit'),
  );

  $header = array(
    'status' => t('Status'),
    'name' => t('Table name'),
    'sql' => t('SQL'),
    'operations' => t('Operations'),
  );

  $options = array();
  if (!empty($tables)) {
    foreach ($tables as $table => $data) {
      if (isset($entity_tables[$table])) {
        // Show here single tables only.
        continue;
      }

      $status = empty($db_sanitizer_conf[$table]['status']) ? DB_SANITIZER_UNDEFINED_STATUS : $db_sanitizer_conf[$table]['status'];
      $sql = empty($db_sanitizer_conf[$table]['commands']) ? '' : db_sanitizer_create_table_sql($db_sanitizer_conf[$table], 'html', FALSE);

      $options[$table] = array(
        'status' => array(
          'data' => $statuses[$status],
          'class' => 'db_sanitizer-status-' . $status,
        ),
        'name' => $table,
        'sql' => $sql,
        'operations' => l(t('edit'), DB_SANITIZER_PATH . '/manage/' . $config_name . '/table_config/' . $table),
      );
    }
  }

  $form['tables'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No tables available.'),
  );

  $form['config_name'] = array(
    '#type' => 'hidden',
    '#value' => $config_name,
  );

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'db_sanitizer') . '/theme/db_sanitizer.css',
  );

  return $form;
}

/**
 * Set button callback.
 */
function db_sanitizer_table_config_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['tables'])) {
    foreach ($form_state['values']['tables'] as $table => $value) {
      if (!empty($value)) {
        $table_config = db_sanitizer_get_table_config($form_state['values']['config_name'], $table);
        if (!isset($table_config['commands']) && $form_state['values']['status'] == DB_SANITIZER_ENABLED_STATUS) {
          // Prevent enable table config if there aren't any command.
          drupal_set_message(t('Status of table config %table has not been changed because it does not contain any commands',
            array('%table' => $table)), 'warning');
        }
        else {
          $table_config['status'] = $form_state['values']['status'];
          db_sanitizer_set_table_config($form_state['values']['config_name'], $table, $table_config);
          drupal_set_message(t('Status of table config %table has been updated', array('%table' => $table)));
        }
      }
    }
  }
}

/**
 * Download sql file handler.
 */
function db_sanitizer_download_sql($name) {
  $output = db_sanitizer_generate_sql($name);
  if (is_array($output)) {
    // Errors.
    foreach ($output as $error) {
      drupal_set_message(check_plain(t($error['message'], $error['variables'])), 'error');
    }
    drupal_goto(DB_SANITIZER_PATH);
  }
  else {
    $filename = tempnam(file_directory_temp(), 'sql');
    file_put_contents($filename, $output);

    drupal_add_http_header('Content-type', 'application/octet-stream');
    drupal_add_http_header('Content-Disposition', 'attachment; filename="db_sanitizer_' . $name . '.sql"');
    drupal_add_http_header('Content-Length', filesize($filename));

    readfile($filename);
    unlink($filename);
  }
}

/**
 * Edit table config form.
 */
function db_sanitizer_table_config_edit_form($form, &$form_state, $name, $table) {
  $tables = db_sanitizer_list_tables();
  if (!isset($tables[$table])) {
    // Print 404 error if we don't have table in the database.
    drupal_not_found();
    drupal_exit();
  }
  $db_sanitizer_conf = db_sanitizer_get_table_config($name, $table);

  $form['#name'] = $name;

  $form['query'] = array(
    '#type' => 'fieldset',
    '#title' => t('Generated query'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['query']['value'] = array(
    '#markup' => db_sanitizer_create_table_sql($db_sanitizer_conf, 'html', FALSE),
  );

  db_sanitizer_table_info_form($tables[$table], $form);
  db_sanitizer_table_commands_form($table, $form, $form_state, $db_sanitizer_conf);

  $form['name'] = array(
    '#type' => 'hidden',
    '#value' => $table,
  );

  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#options' => db_sanitizer_get_statuses(),
    '#default_value' => isset($db_sanitizer_conf['status']) ? $db_sanitizer_conf['status'] : DB_SANITIZER_UNDEFINED_STATUS,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#validate' => array('db_sanitizer_config_edit_form_validate'),
  );

  $form['cancel'] = array(
    '#markup' => l(t('Cancel'), DB_SANITIZER_PATH . '/manage/' . $name . '/table_config'),
  );

  return $form;
}

/**
 * Edit table config form submit.
 */
function db_sanitizer_table_config_edit_form_submit($form, &$form_state) {
  $config = array(
    'name' => $form_state['values']['name'],
    'status' => $form_state['values']['status'],
  );

  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value)) {
      if (!empty($value['commands'])) {
        foreach ($value['commands'] as $command) {
          if (!$command['remove']) {
            // Save command.
            unset($command['remove']);
            $config['commands'][] = $command;
          }
        }
      }      
    }
  }
  db_sanitizer_set_table_config($form['#name'], $config['name'], $config);
  drupal_set_message(t('Table config has been updated.'));
}

/**
 * Add command submit.
 */
function db_sanitizer_table_config_edit_form_add_command_submit($form, &$form_state) {
  $command = array();
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, 'op_') === 0) {
      $table = str_replace('op_', '', $key);
      break;
    }
  }
  // System condition examples.
  $conditions = array();
  if (!empty($form_state['values'][$table]['base_table']) && !empty($form['#bundle']) && !empty($form_state['values'][$table]['bundle_key'])) {
    // Base table.
    $conditions[] = _db_sanitizer_base_table_condition2($form_state['values'][$table]['bundle_key'], $form['#bundle']);
  }
  if (empty($form_state['values'][$table]['base_table']) && !empty($form['#entity_type']) && (!empty($form['#bundle']))) {
    // Data table.
    $conditions[] = _db_sanitizer_data_table_condition2($form['#entity_type'], $form['#bundle']);
  }
  $condition = implode(' AND ', $conditions);
  switch ($value) {
    case t('Add TRUNCATE command'):
      $command['type'] = 'truncate';
      break;
    case t('Add UPDATE command'):
      $command['type'] = 'update';
      $command['set'] = '';
      $command['condition'] = '';
      $command['condition2'] = $condition;
      break;
    case t('Add DELETE command'):
      $command['type'] = 'delete';
      $command['condition'] = '';
      $command['condition2'] = $condition;
      break;
  }

  $form_state['add_command'][$table][] = $command;
  $form_state['rebuild'] = TRUE;
}

/**
 * Add command ajax callback.
 */
function db_sanitizer_table_config_edit_form_add_command_callback($form, $form_state) {
  //$table = $form_state['triggering_element']['#ajax']['table'];
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, 'op_') === 0) {
      $table = str_replace('op_', '', $key);
    }
  }
  return $form[$table]['commands'];
}

/**
 * Generates fields for commands.
 */
function db_sanitizer_table_config_edit_command_fields($command) {
  $form = array();

  $form['remove'] = array(
    '#type' => 'checkbox',
    '#default_value' => 0,
  );
  $form['type'] = array(
    '#type' => 'hidden',
    '#value' => $command['type'],
  );
  $form['type_label'] = array(
    '#markup' => check_plain($command['type']),
  );
  if (isset($command['set'])) {
    $form['set'] = array(
      '#type' => 'textfield',
      '#default_value' => $command['set'],
      '#description' => t('Expression after SET statement'),
      '#required' => TRUE,
      '#maxlength' => 2000,
    );
  }
  if (isset($command['condition'])) {
    $form['condition'] = array(
      '#type' => 'textfield',
      '#default_value' => $command['condition'],
      '#description' => t('Expression after WHERE statement'),
      '#maxlength' => 2000,
    );
  }
  if (isset($command['condition2'])) {
    $form['condition2'] = array(
      '#markup' => $command['condition2']
    );
  }
  return $form;
}

/**
 * Adds table information on a form.
 */
function db_sanitizer_table_info_form($data, &$form) {
  $row_header = array(t('Name'), t('Type[:Size]'), t('Null?'), t('Default'));
  $rows = array();
  foreach ($data['fields'] as $c_name => $c_spec) {
    $row = array();
    $row[] = $c_name;
    $type = $c_spec['type'];
    if (!empty($c_spec['length'])) {
      $type .= '(' . $c_spec['length'] . ')';
    }
    if (!empty($c_spec['scale']) && !empty($c_spec['precision'])) {
      $type .= '(' . $c_spec['precision'] . ', ' . $c_spec['scale'] . ' )';
    }
    if (!empty($c_spec['size']) && $c_spec['size'] != 'normal') {
      $type .= ':' . $c_spec['size'];
    }
    if ($c_spec['type'] == 'int' && !empty($c_spec['unsigned'])) {
      $type .= ', unsigned';
    }
    $row[] = $type;
    $row[] = !empty($c_spec['not null']) ? 'NO' : 'YES';
    $row[] = isset($c_spec['default']) ? (is_string($c_spec['default']) ? '\'' . $c_spec['default'] . '\'' : $c_spec['default']) : '';
    $rows[] = $row;
  }
  if (!empty($data['description'])) {
    $rows[] = array(array('colspan' => count($row_header), 'data' => $data['description']));
  }

  // Table info.
  $form[$data['name']] = array(
    '#type' => 'fieldset',
    '#title' => t('%table table', array('%table' => $data['name'])),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form[$data['name']]['content'] = array(
    '#theme' => 'table',
    '#header' => $row_header,
    '#rows' => $rows,
  );
}

/**
 * Constructs Commands fieldset.
 */
function db_sanitizer_table_commands_form($table, &$form, $form_state, $db_sanitizer_conf = NULL) {
  // Commands.
  $form[$table]['commands'] = array(
    '#theme' => 'db_sanitizer_table_config_edit_form_commands',
    '#tree' => TRUE,
    '#prefix' => '<div id="db_sanitizer-commands-wrapper-' . $table . '">',
    '#suffix' => '</div>',
    '#title' => t('Commands'),
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $i = 0;
  if (!empty($db_sanitizer_conf['commands'])) {
    foreach ($db_sanitizer_conf['commands'] as $command) {
      $form[$table]['commands'][$i] = db_sanitizer_table_config_edit_command_fields($command);
      ++$i;
    }
  }

  if (!empty($form_state['add_command'][$table])) {
    foreach ($form_state['add_command'][$table] as $command) {
      $form[$table]['commands'][$i] = db_sanitizer_table_config_edit_command_fields($command);
      ++$i;
    }
  }
  // Add commands button.
  // TRUNCATE command.
  $form[$table]['truncate_command'] = array(
    '#type' => 'submit',
    '#value' => t('Add TRUNCATE command'),
    '#submit' => array('db_sanitizer_table_config_edit_form_add_command_submit'),
    '#name' => 'op_' . $table,
    '#ajax' => array(
      'callback' => 'db_sanitizer_table_config_edit_form_add_command_callback',
      'wrapper' => 'db_sanitizer-commands-wrapper-' . $table,
      // 'table' => $table,
    )
  );
  // UPDATE command.
  $form[$table]['update_command'] = array(
    '#type' => 'submit',
    '#value' => t('Add UPDATE command'),
    '#submit' => array('db_sanitizer_table_config_edit_form_add_command_submit'),
    '#name' => 'op_' . $table,
    '#ajax' => array(
      'callback' => 'db_sanitizer_table_config_edit_form_add_command_callback',
      'wrapper' => 'db_sanitizer-commands-wrapper-' . $table,
    )
  );
  // DELETE command.
  $form[$table]['delete_command'] = array(
    '#type' => 'submit',
    '#value' => t('Add DELETE command'),
    '#submit' => array('db_sanitizer_table_config_edit_form_add_command_submit'),
    '#name' => 'op_' . $table,
    '#ajax' => array(
      'callback' => 'db_sanitizer_table_config_edit_form_add_command_callback',
      'wrapper' => 'db_sanitizer-commands-wrapper-' . $table,
    )
  );
}

/**
 * Entity config page.
 */
function db_sanitizer_entity_config_form($form, &$form_state, $config_name) {
  $entity_config = db_sanitizer_get_entity_config($config_name);
  $statuses = db_sanitizer_get_statuses();

  $form['operations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Table operations'),
  );
  $form['operations']['status'] = array(
    '#type' => 'select',
    '#title' => t('Set status'),
    '#options' => $statuses,
  );
  $form['operations']['set'] = array(
    '#type' => 'submit',
    '#value' => t('Set'),
    '#submit' => array('db_sanitizer_entity_config_form_submit'),
  );

  $header = array(
    'status' => t('Status'),
    'entity_label' => t('Entity'),
    'bundle_label' => t('Bundle'),
    'sql' => t('SQL'),
    'operations' => t('Operations')
  );

  $options = array();
  foreach (entity_get_info() as $entity_type => $entity) {
    foreach ($entity['bundles'] as $bundle_key => $bundle) {
      $status = empty($entity_config[$entity_type][$bundle_key]['status']) ? DB_SANITIZER_UNDEFINED_STATUS : $entity_config[$entity_type][$bundle_key]['status'];
      $sql = '';

      if (!empty($entity_config[$entity_type][$bundle_key]['tables'])) {
        foreach ($entity_config[$entity_type][$bundle_key]['tables'] as $config) {
          $sql .= db_sanitizer_create_table_sql($config, 'html', FALSE);
        }
      }

      $options[$entity_type . '|' . $bundle_key] = array(
        'status' => array(
          'data' => $statuses[$status],
          'class' => 'db_sanitizer-status-' . $status,
        ),
        'entity_label' => $entity['label'],
        'bundle_label' => $bundle['label'],
        'sql' => $sql,
        'operations' => l(t('edit'), DB_SANITIZER_PATH . '/manage/' . $config_name . '/entity_config/' . $entity_type . '/' . $bundle_key),
      );
    }
  }

  $form['entities'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No tables available.'),
  );

  $form['config_name'] = array(
    '#type' => 'hidden',
    '#value' => $config_name,
  );

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'db_sanitizer') . '/theme/db_sanitizer.css',
  );

  return $form;
}

/**
 * Set button callback.
 */
function db_sanitizer_entity_config_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['entities'])) {
    foreach ($form_state['values']['entities'] as $key => $value) {
      if ($value) {
        list($entity_type, $bundle) = explode('|', $key);
        $entity_config = db_sanitizer_get_entity_config($form_state['values']['config_name'], $entity_type, $bundle);

        if (!isset($entity_config['tables']) && $form_state['values']['status'] == DB_SANITIZER_ENABLED_STATUS) {
          drupal_set_message(t('Status of entity config %entity with bundle %bundle
           has not been changed becuse it does not contain any commands',
            array('%entity' => $entity_type, '%bundle' => $bundle)),
            'warning');
        }
        else {
          $entity_config['status'] = $form_state['values']['status'];
          db_sanitizer_set_entity_config($form_state['values']['config_name'], $entity_type, $bundle, $entity_config);
          drupal_set_message(t('Status of entity config %entity with bundle %bundle has been updated',
            array('%entity' => $entity_type, '%bundle' => $bundle)));
        }
      }
    }
  }
}

/**
 * Update entity config.
 */
function db_sanitizer_entity_edit_form($form, &$form_state, $name, $entity_type, $bundle) {
  $entity_info = entity_get_info($entity_type);
  if (!$entity_info) {
    drupal_not_found();
    drupal_exit();
  }

  $tables = db_sanitizer_list_tables();
  $entity_config = db_sanitizer_get_entity_config($name, $entity_type, $bundle);

  $form['#bundle'] = $bundle;
  $form['#entity_type'] = $entity_type;
  $form['#name'] = $name;

  $form['query'] = array(
    '#type' => 'fieldset',
    '#title' => t('Generated query'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['query']['value'] = array(
    '#markup' => db_sanitizer_create_entity_sql($entity_type, $bundle, $entity_config, 'html', FALSE),
  );

  // Base table.
  $base_table = $entity_info['base table'];
  db_sanitizer_table_info_form($tables[$base_table], $form);
  if (!empty($entity_config['tables'][$base_table])) {
    db_sanitizer_table_commands_form($base_table, $form, $form_state, $entity_config['tables'][$base_table]);
  }
  else {
    db_sanitizer_table_commands_form($base_table, $form, $form_state);
  }
  
  unset($form[$base_table]['truncate_command']);

  $form[$base_table]['name'] = array(
    '#type' => 'hidden',
    '#value' => $base_table,
  );
  $form[$base_table]['base_table'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );
  if (!empty($entity_info['bundle keys']['bundle']) && $entity_type != 'taxonomy_term') {
    $form[$base_table]['bundle_key'] = array(
      '#type' => 'hidden',
      '#value' => $entity_info['bundle keys']['bundle'],
    );
  }
  if (!empty($entity_info['revision table'])) {
    $form[$base_table]['revision table'] = array(
      '#type' => 'hidden',
      '#value' => $entity_info['revision table'],
    );
  }
  if (!empty($entity_info['entity keys']['id'])) {
    $form[$base_table]['entity_key'] = array(
      '#type' => 'hidden',
      '#value' => $entity_info['entity keys']['id'],
    );
  }
  if ($entity_info['fieldable']) {
    // Entity has fields so show its tables.
    $instances = field_info_instances($entity_type, $bundle);

    foreach ($instances as $name => $instance) {
      $field_info = field_info_field($instance['field_name']);
      if ($field_info) {
        $data_table = key($field_info['storage']['details']['sql'][FIELD_LOAD_CURRENT]);
        $revision_table = key($field_info['storage']['details']['sql'][FIELD_LOAD_REVISION]);

        db_sanitizer_table_info_form($tables[$data_table], $form);
        if (!empty($entity_config['tables'][$data_table])) {
          db_sanitizer_table_commands_form($data_table, $form, $form_state, $entity_config['tables'][$data_table]);
        }
        else {
          db_sanitizer_table_commands_form($data_table, $form, $form_state);
        }
        unset($form[$data_table]['truncate_command']);
        unset($form[$data_table]['delete_command']);

        $form[$data_table]['name'] = array(
          '#type' => 'hidden',
          '#value' => $data_table,
        );
        $form[$data_table]['bundle_key'] = array(
          '#type' => 'hidden',
          '#value' => 'bundle',
        );
        $form[$data_table]['revision table'] = array(
          '#type' => 'hidden',
          '#value' => $revision_table,
        );
      }
    }
  }

  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#options' => db_sanitizer_get_statuses(),
    '#default_value' => isset($entity_config['status']) ? $entity_config['status'] : 0 ,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#validate' => array('db_sanitizer_config_edit_form_validate'),
  );

  $form['cancel'] = array(
    '#markup' => l(t('Cancel'), DB_SANITIZER_PATH . '/manage/' . $name . '/entity_config'),
  );

  return $form;
}

/**
 * Edit config form validate.
 */
function db_sanitizer_config_edit_form_validate($form, &$form_state) {
  if (isset($form_state['values']['op'])) {
    $count_commands = 0;
    foreach ($form_state['values'] as $key => $value) {
      if (is_array($value)) {
        if (!empty($value['commands'])) {
          foreach ($value['commands'] as $command) {
            if (!$command['remove']) {
              ++$count_commands;
            }
          }
        }
      }
    }
    if (!$count_commands && $form_state['values']['status'] == DB_SANITIZER_ENABLED_STATUS) {
      form_set_value($form['status'], DB_SANITIZER_UNDEFINED_STATUS, $form_state);
      $statuses = db_sanitizer_get_statuses();
      drupal_set_message(t('Status has been set as @status because you do not have any commands', array('@status' => $statuses[DB_SANITIZER_UNDEFINED_STATUS])), 'warning');
    }
  }
}

/**
 * Edit entity config form submit.
 */
function db_sanitizer_entity_edit_form_submit($form, &$form_state) {
  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];

  $config = array(
    'status' => $form_state['values']['status'],
  );

  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value)) {
      if (!empty($value['commands'])) {
        foreach ($value['commands'] as $command) {
          if (!$command['remove']) {
            $config['tables'][$value['name']]['name'] = $value['name'];
            if (!empty($value['base_table'])) {
              $config['tables'][$value['name']]['base_table'] = $value['base_table'];
            }
            // Save command.
            unset($command['remove']);
            $config['tables'][$value['name']]['commands'][] = $command;
          }
        }
      }
    }
  }

  db_sanitizer_set_entity_config($form['#name'], $entity_type, $bundle, $config);
  drupal_set_message(t('Entity config has been updated.'));
}

/**
 * Title callback for manage/% path.
 */
function db_sanitizer_manage_title($name) {
  $db_sanitizer_configuration = db_sanitizer_get_configurations($name);
  if (!empty($db_sanitizer_configuration)) {
    $label = $db_sanitizer_configuration['label'];
  }
  else {
    $label = t('Unknown');
  }

  return t('@label configuration', array('@label' => $label));
}

/**
 * Table config edit page title.
 */
function db_sanitizer_table_config_edit_title($table_name) {
  $tables = db_sanitizer_list_tables();
  if (isset($tables[$table_name])) {
    $table = $tables[$table_name]['name'];
  }
  else {
    $table = t('Unknown');
  }

  return t('@table table', array('@table' => $table));
}

/**
 * Title callback for db_sanitizer_entity_edit_form page.
 */
function db_sanitizer_entity_config_edit_title($entity_type, $bundle) {
  $entity_info = entity_get_info($entity_type);
  foreach ($entity_info['bundles'] as $bundle_key => $bundle_value) {
    if ($bundle_key == $bundle) {
      return $entity_info['label'] . ' - ' . $bundle_value['label'];
    }
  }
  return '';
}

/**
 * Config's statuses.
 */
function db_sanitizer_get_statuses() {
  return array(
    DB_SANITIZER_UNDEFINED_STATUS => t('Undefined'),
    DB_SANITIZER_DISABLED_STATUS => t('Disabled'),
    DB_SANITIZER_ENABLED_STATUS => t('Enabled'),
  );
}
