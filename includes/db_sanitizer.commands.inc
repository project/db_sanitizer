<?php
/**
 * @file
 * Command classes.
 */


class SanitizerCommand {
  protected $table;
  protected $config;
  protected $sql;

  public function __construct($table, $config) {
    $this->table = $table;
    $this->config = $config;
  }

  protected function generateSQL() {

  }

  // Adds condition.
  protected function addCondition() {
    if (!empty($this->config['condition'])) {
      $this->sql .= ' WHERE ' . $this->config['condition'];
    }
  }

  public function getSQLString() {
    $this->sql .= ';';
    return $this->sql;
  }

}

/**
 * Class SanitizerCommandTruncate
 * Generates TRUNCATE sql command.
 */
class SanitizerCommandTruncate extends SanitizerCommand{
  protected function generateSQL() {
    $this->sql = 'TRUNCATE TABLE `' . $this->table . '`';
  }

  public function getSQLString() {
    $this->generateSQL();
    return parent::getSQLString();
  }
}

/**
 * Class SanitizerCommandDelete
 * Generates DELETE sql command.
 */
class SanitizerCommandDelete extends SanitizerCommand{
  protected function generateSQL() {
    $this->sql = 'DELETE FROM `' . $this->table . '`';
    $this->addCondition();
  }

  public function getSQLString() {
    $this->generateSQL();
    return parent::getSQLString();
  }
}

/**
 * Class SanitizerCommandUpdate.
 * Generated Update sql command.
 */
class SanitizerCommandUpdate extends SanitizerCommand{
  protected function generateSQL() {
    $this->sql = 'UPDATE `' . $this->table . '`';
    if (!empty($this->config['set'])) {
      $this->sql .= ' SET ' . $this->config['set'];
    }
    $this->addCondition();
  }

  public function getSQLString() {
    $this->generateSQL();
    return parent::getSQLString();
  }
}
